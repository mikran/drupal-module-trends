import React from "react"

function Header() {
  return (
    <header className="navbar">
      Drupal Module Trends
    </header>
  )
}

export default Header
